import { loadRemoteEntry } from '@angular-architects/module-federation';

Promise.all([
  loadRemoteEntry('http://localhost:5000/remoteEntry.js', 'collateral'),
  loadRemoteEntry('http://localhost:5001/remoteEntry.js', 'customer'),
])
  .catch((err) => console.error('Error loading remote entries', err))
  .then(() => import('./bootstrap'))
  .catch((err) => console.error(err));

import { Component } from '@angular/core';

@Component({
  selector: 'eubv3-host-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'eubv3-host';
}

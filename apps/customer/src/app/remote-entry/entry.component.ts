import { Component } from '@angular/core';

@Component({
  selector: 'customer-entry',
  template: `<div class="remote-entry">
    <h2>customer Remote Entry Component</h2>
  </div>`,
  styles: [
    `
      .remote-entry {
        padding: 5px;
        color: white;
        background-color: #143055;
      }
    `,
  ],
})
export class RemoteEntryComponent {}

import { Component } from '@angular/core';

@Component({
  selector: 'collateral-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'collateral';
}
